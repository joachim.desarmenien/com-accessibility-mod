(($, Drupal) => {
  function replaceTag(elt, tagName) {
    return elt.replaceWith(() => {
      const { attributes } = elt[0];
      const replacement = $(`<${tagName}>`).html(elt.html());
      for (let i = 0; i < attributes.length; i++) {
        replacement.attr(attributes[i].name, attributes[i].value);
      }
      return replacement;
    });
  }
  function updateDialog() {
    $.widget('ui.dialog', $.ui.dialog, {
      _create() {
        this._super();
        this.uiDialog.attr('autofocus', 'autofocus');
        if (this.options.modal) {
          // add  aria-modal and remove aria-describedby attributes
          // https://git.scnbdx.fr/sid/drupal/modules/com-bootstrap5-theme/-/issues/34
          this.uiDialog
            .attr('aria-modal', 'true')
            .removeAttr('aria-describedby');
        }
      },
      _createTitlebar() {
        this._super();
        const h1 = replaceTag(
          this.uiDialogTitlebar.find('.ui-dialog-title'),
          'h1',
        );
        this.uiDialogTitlebarClose.attr('aria-describedby', h1.attr('id'));
        if (this.uiDialogTitlebarClose.text() === '') {
          this.uiDialogTitlebarClose.append(
            `<span class="visually-hidden sr-only">${this.options.closeText}</span>`,
          );
        }
      },
    });
  }
  Drupal.behaviors.bm_term_glossary_accessibility = {
    attach() {
      updateDialog();
    },
  };
})(jQuery, Drupal);
