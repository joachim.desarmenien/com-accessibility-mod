/**
 * @file
 * JavaScript's behaviors for select2 to add aria-label.
 */

(($, Drupal, once) => {
  Drupal.behaviors.CustomSelect2Form = {
    attach(context) {
      const formSelectElements = context.querySelectorAll(
        '.select2-selection__rendered',
      );
      if (formSelectElements.length > 0) {
        formSelectElements.forEach((value) => {
          const parentSpan = value.parentElement;
          const div = value.closest('div');
          const label = div.querySelector('label');
          const labelFor = `${label.getAttribute('for')}-label`;

          parentSpan.setAttribute('role', 'button');
          parentSpan.removeAttribute('aria-haspopup');
          label.setAttribute('id', labelFor);

          once(label.id, div).forEach(() => {
            const currentAriaLabelledBy =
              parentSpan.getAttribute('aria-labelledby') || '';
            parentSpan.setAttribute(
              'aria-labelledby',
              `${currentAriaLabelledBy} ${labelFor}`.trim(),
            );
          });
        });
      }

      const processSelect2Element = (element) => {
        once('select2-handling', element).forEach((val) => {
          val.addEventListener(
            'click',
            () => {
              const label = val.closest('div').querySelector('label').innerText.trim();
              const input = document.querySelector('.select2-search__field');
              input.setAttribute('aria-label', Drupal.t('Search: @caption', {
                '@caption': label,
              }));
              input.setAttribute('title', Drupal.t('Search: @caption', {
                '@caption': label,
              }));
            },
            { passive: true },
          );
        });
      };
      const observer = new MutationObserver((mutationsList) => {
        mutationsList.forEach((mutation) => {
          if (mutation.type === 'childList') {
            mutation.addedNodes.forEach((node) => {
              if (node.nodeType === Node.ELEMENT_NODE) {
                if (node.matches('.select2-selection__rendered')) {
                  processSelect2Element(node);
                }
                const nestedSelect2Elements = node.querySelectorAll(
                  '.select2-selection__rendered',
                );
                nestedSelect2Elements.forEach(processSelect2Element);
              }
            });
          }
        });
      });
      observer.observe(context, { childList: true, subtree: true });
      const existingSelect2Elements = context.querySelectorAll(
        '.select2-selection__rendered',
      );
      existingSelect2Elements.forEach(processSelect2Element);
    },
  };
})(jQuery, Drupal, once);
