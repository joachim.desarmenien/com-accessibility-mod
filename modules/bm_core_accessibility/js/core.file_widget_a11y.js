/**
 * @file
 * Javascript additionnel dans la gestion des uploads de fichier.
 * Permet de positionner le focus sur le fichier après l'upload.
 */
((Drupal, drupalSettings, once) => {
  Drupal.behaviors.bm_core_accessibility_file_widget = {
    attach(context, settings) {
      const fileWidgets = settings.bm_core_accessibility.fileWidgets;
      for (const fileWidget of Object.values(fileWidgets)) {
        const { action, multiValue, name } = fileWidget;
        const dashedSlug = name.replaceAll('_', '-');
        const wrapper = context.querySelector(`.form-item-${dashedSlug}`);
        let focusElement;
        if (action === 'upload') {
          const deleteButton = wrapper.querySelector('input[type=submit][name$="_remove_button"]');
          if (deleteButton) {
            if (multiValue) {
              // TODO: aria-label multiple.
              focusElement = wrapper.querySelector('.form-managed-file label a') ? wrapper.querySelector('.form-managed-file label a') : wrapper.querySelector('.form-managed-file label');
              if (!focusElement) {
                focusElement = deleteButton;
              }
            }
            else {
              const label = wrapper.querySelector('span.file');
              if (label) {
                deleteButton.setAttribute(
                  'aria-label',
                  `${deleteButton.value} ${label.innerText.trim()}`
                );
                focusElement = label.querySelector('a');
                if (!focusElement) {
                  focusElement = deleteButton;
                }
              }
            }
          }
        }
        else if (action === 'remove') {
          focusElement = wrapper.querySelector('input[type=file]');
        }

        if (focusElement) {
          focusElement.focus();
        }
      }
    },
  };
})(Drupal, drupalSettings, once);
